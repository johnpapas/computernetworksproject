package projectCN;
import ithakimodem.Modem;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;


public class Main {
	
	//insert the request codes manually
	
	private static final String echoRequestCode="\r";
	
	private static final String imageRequestCode="\r";
	
	private static final String errorImageRequestCode="\r";
	
	private static final String GPSRequestCode="";
	
	private static final String ACKRequestCode="\r";
	
	private static final String NACKRequestCode="\r";
	
	public static void main(String[] args) throws InterruptedException {
		int numberOfImages=0;
		Modem modem=new Modem();
		modem.open("ithaki");
		modem.setSpeed(90000);
		int timeout=2000;
		modem.setTimeout(timeout);
		showServerIncomingMessage(modem);		
		
		EchoRequest echoRequest=new EchoRequest(echoRequestCode);
		
		if(echoRequest.getEchoRequestCode().startsWith("E"))
		echoPackagesProcess(modem, timeout, echoRequest);
		
		ImageRequest imageRequest=new ImageRequest(imageRequestCode);
		
		if(imageRequest.getImageRequestCode().startsWith("M"))
		numberOfImages=imageProcess(modem, imageRequest, numberOfImages);
		
		ImageRequest errorImageRequest=new ImageRequest(errorImageRequestCode);
		
		if(errorImageRequest.getImageRequestCode().startsWith("G"))
		numberOfImages=imageProcess(modem, errorImageRequest, numberOfImages);
		
		GPSRequest gpsRequest=new GPSRequest(GPSRequestCode);
		
		if(gpsRequest.getGPSRequestCode().startsWith("P"))
		GPSTracking(modem, gpsRequest, numberOfImages);
		
		AcknowledgementRequest ackRequest=new AcknowledgementRequest(ACKRequestCode,NACKRequestCode);		
		
		if((ackRequest.getACKRequestCode().startsWith("Q")) && (ackRequest.getNACKRequestCode().startsWith("R")))
		ARQProcess(modem, ackRequest, timeout);
						
		modem.close();
	}
	
	//The process for the echoPackages
	
	public static void echoPackagesProcess(Modem modem, int timeout, EchoRequest echoRequest) {
		long timePassed=0;
		ArrayList<Long> pings=new ArrayList<Long>();
		while(timePassed<240000) { //echoPackages for 4 minutes 
			long begin=System.currentTimeMillis();
			modem.write(echoRequest.getEchoRequestCode().getBytes());
			showEchoPackages(modem);
			long end=System.currentTimeMillis();
			System.out.println(" " + (end-begin) +"ms");//shows each ping for each packet
			pings.add(end-begin);
			timePassed=timePassed+(end-begin);
		}
		//saves the pings at a file, which directory is shown afterwards
		try {
			File pingsFile=new File("Pings.txt");
			FileOutputStream out=new FileOutputStream(pingsFile);
			for(int i=0;i<pings.size();i++) {
				out.write((Long.toString(pings.get(i))+"\n").getBytes());
			}
			System.out.println("Pings saved at " + pingsFile.getCanonicalPath());
			out.close();
		}
		catch(Exception x) {
			x.printStackTrace();
		}
		
	}
	
	//The process for saving the images
	
	public static int imageProcess(Modem modem, ImageRequest imageRequest, int k) {
		modem.write(imageRequest.getImageRequestCode().getBytes()); //sending the request code to the server
		writeImage(modem,k);
		k++;
		return k;
	}
	
	//The automatic repeat request and saves the pings and attempts for every package at a file
	
	public static void ARQProcess(Modem modem, AcknowledgementRequest ackRequest, int timeout) {
		long timePassed=0;
		ArrayList<Long> ARQpings=new ArrayList<Long>();
		ArrayList<Integer> attempts=new ArrayList<Integer>();
		while(timePassed<240000) { //echoPackages for 4 minutes
			long begin=System.currentTimeMillis();
			int attempt=ARQ(modem, ackRequest.getACKRequestCode(), ackRequest.getNACKRequestCode());
			long end=System.currentTimeMillis();
			System.out.println((end-begin) +"ms");
			ARQpings.add(end-begin);
			attempts.add(attempt);
			timePassed=timePassed+(end-begin);
		}
		//saves both the pings and the attempts for the packages at a text file, and then shows its dir afterwards
		try {
			File ARQPingsFile=new File("ARQPings.txt");
			FileOutputStream out=new FileOutputStream(ARQPingsFile);
			for(int i=0;i<ARQpings.size();i++) {
				out.write((Long.toString(ARQpings.get(i))+"\t"+Integer.toString(attempts.get(i))+"\n").getBytes());
			}
			System.out.println("The attempts and the pings for the ARQ saved at " + ARQPingsFile.getCanonicalPath());
			out.close();
		}
		catch(Exception x) {
			x.printStackTrace();
		}
	}
	
	//Shows the introduction message, or used for showing anything that is sent from the server
	
	public static void showServerIncomingMessage(Modem modem) {
		int k;
		while(true) {
			try {
				k=modem.read();
				if(k==-1) break;
				System.out.print((char)k);
			}
			catch(Exception x) {
				break;
			}
		}
	}
	
	//Shows the Echo Packages downwards
	
	public static void showEchoPackages(Modem modem) {
		String message="";
		int k;
		while(true) {
			try {
				k=modem.read();
				message=message+(char)k;
				if(message.endsWith("PSTOP")) {
					System.out.print(message);
					message="";
					break;
				}
				if(k==-1) break;
			}
			catch(Exception x) {
				break;
			}
		}
	}
	
	//Saves the incoming image at a file
	
	public static void writeImage(Modem modem, int number) {
		FileOutputStream out= null;
		String fileName="image"+Integer.toString(number)+".jpeg";
		File imageFile=new File(fileName);
		System.out.println("Saving image...");
		try {
			out=new FileOutputStream(imageFile);
			int k;
			while(true) {
				k=modem.read();
				out.write(k);
				if(k==-1) break;
			}
			System.out.println("Image saved at " + imageFile.getCanonicalPath());
		}
		catch(Exception x) {
			x.printStackTrace();
		}
	}

	//Checks if the incoming package is correct, and if not it requests to send another one
	
	public static int ARQ(Modem modem, String ACK, String NACK) {
		modem.write(ACK.getBytes());
		int attempts=0;
		String message="";
		int[] encryptedChars=new int[16];
		String FCS="";
		int k;
		while(true) {
			attempts++;
			FCS="";
			message="";
			try {
				while(true) {
					k=modem.read();
					message=message+(char)k;
					if(message.endsWith("<")) {
						for(int i=0;i<16;i++) {
							k=modem.read();
							encryptedChars[i]=k;
							message=message+(char)k;
						}
						k=modem.read();
						message=message+(char)k;
						k=modem.read();
						message=message+(char)k;
						for(int i=0;i<3;i++) {
							k=modem.read();
							FCS=FCS+(char)k;
							message=message+(char)k;
						}
					}
					if(message.endsWith("PSTOP")) {
						System.out.print(message);
						break;
					}
					if(k==-1) break;
				}
			}
			catch(Exception x) {
				x.printStackTrace();
			}
			int result=0;
			for(int i=0;i<16;i++) {
				result=result ^ encryptedChars[i];
			}
			
			if(result==Integer.parseInt(FCS)) {
				System.out.println("\tOK");
				break;
			}
			else {
				System.out.println("\tNOT OK");
				modem.write(NACK.getBytes());
				continue;
			}
		}
		return attempts;
	}
	
	//Sends the request for the GPS coordinates, and then it sends the coordinates and saves the image with the coordinates from the map
	
	public static void GPSTracking(Modem modem, GPSRequest gpsRequest, int n) {
		String secondRequest=gpsRequest.getGPSRequestCode();
		String firstRequest=gpsRequest.getGPSRequestCode()+"R=1002070\r";
		modem.write(firstRequest.getBytes());
		String message="";
		int k;
		while(true) {
			try {
				k=modem.read();
				message=message+(char)k;
				if(k==-1) break;
			}
			catch(Exception x) {
				break;
			}
		}
		System.out.println(message);
		String[] splittedMessage=message.split(",");
		for(int i=0;i<70*14;i=i+5*14) {
			String[] latitude=splittedMessage[i+2].split("\\.");
			String[] longtitude=splittedMessage[i+4].split("\\.");
			String latitudeDeg=latitude[0].substring(0, 2);
			String latitudeMin=latitude[0].substring(2, 4);
			double latitudeSec=0.006*Integer.parseInt(latitude[1]);
			String latitudeSecs=Integer.toString((int)latitudeSec);
			String longtitudeDeg=longtitude[0].substring(1,3);
			String longtitudeMin=longtitude[0].substring(3, 5);
			double longtitudeSec=0.006*Integer.parseInt(longtitude[1]);
			String longtitudeSecs=Integer.toString((int)longtitudeSec);
			
			secondRequest=secondRequest+"T="+longtitudeDeg+longtitudeMin+longtitudeSecs+latitudeDeg+latitudeMin+latitudeSecs;
			}
		secondRequest=secondRequest+"\r";
		modem.write(secondRequest.getBytes());
		writeImage(modem, n);
	}

}


