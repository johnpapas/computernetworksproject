The request codes must be put manually before you run the app. 
They must not have spaces at the beginning  or else the given code is not read, and the function for the procedure will not be called.
I have used some "if statements" for checking if the request code is given properly and correct, and if anyone wants to use a function without using all of them.
Also, the code must be written in front of the \r.
Line 67 and Line 107, you can change the time for downloading packets (for example 5min=5min*60s*1000ms=300000ms).
Line 254: you can change the GPS parameter R=1XXXX41, the XXXX is the first track, you can change it so the app gives some other tracks.
After the image codes you can put the parameter CAM=XX, giving the XX(integers) and so you can change the camera used for taking the photos
