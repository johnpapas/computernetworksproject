package projectCN;

public class AcknowledgementRequest {
	private String ACKRequestCode;
	private String NACKRequestCode;
	
	public AcknowledgementRequest(String r1, String r2) {
		ACKRequestCode=r1;
		NACKRequestCode=r2;
	}
	public String getACKRequestCode() {
		return ACKRequestCode;
	}
	public String getNACKRequestCode() {
		return NACKRequestCode;
	}
}
